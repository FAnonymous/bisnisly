package adapter.home;

import com.jfoenix.controls.JFXButton;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataBusinessIdea;
import data.DataUser;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import xml.helper.User;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class HomeAdapterFXML extends ListCell<DataBusinessIdea> implements Initializable {

    @FXML
    private Label txtTitle;

    @FXML
    private Label txtContent;

    @FXML
    private Label txtDesc;

    @FXML
    private JFXButton txtCategory;

    @FXML
    private AnchorPane root;

    private DataBusinessIdea businessIdea;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setGraphic(root);
    }

    public AnchorPane getRoot() {
        return root;
    }

    public static HomeAdapterFXML newInstance() {
        FXMLLoader loader = new FXMLLoader(HomeAdapterFXML.class.getResource("HomeAdapterFXML.fxml"));
        try {
            loader.load();
            return loader.getController();
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    protected void updateItem(DataBusinessIdea item, boolean empty) {
        super.updateItem(item, empty);
        getRoot().getChildrenUnmodifiable().forEach(c -> c.setVisible(!empty));
        if (!empty && item != null && !item.equals(this.businessIdea)) {
            String namaUser = "";
            ArrayList<DataUser> dataUsers = new ArrayList<>();
            XStream xStream = new XStream(new StaxDriver());
            FileInputStream fileInputStream;
            try {
                fileInputStream = new FileInputStream("src/xml/DataUser.xml");
                dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
                fileInputStream.close();

            } catch (Exception e) {
                System.err.println("Terjadi Kesalahan: " + e.getMessage());
            }

            for (int i = 0; i < dataUsers.size(); i++) {
                if (dataUsers.get(i).getId() == item.getUserId()) {
                    namaUser = dataUsers.get(i).getNama();
                }
            }

            txtTitle.textProperty().set(item.getTitle());
            txtContent.textProperty().set(item.getContent());
            txtCategory.textProperty().set(item.getCategory());
            
            if (item.getCategory().equals("Kuliner")) {
                txtCategory.setStyle("-fx-background-color: #FE000F; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Financial")) {
                txtCategory.setStyle("-fx-background-color: #ff6d00; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Jasa")) {
                txtCategory.setStyle("-fx-background-color: #c7a500; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Kesehatan")) {
                txtCategory.setStyle("-fx-background-color: #79b700; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Komunikasi")) {
                txtCategory.setStyle("-fx-background-color: #aeea00; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Konstruksi")) {
                txtCategory.setStyle("-fx-background-color: #2962ff; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Manufaktur")) {
                txtCategory.setStyle("-fx-background-color: #0026ca; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Pertanian")) {
                txtCategory.setStyle("-fx-background-color: #8F14FB; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Perternakan")) {
                txtCategory.setStyle("-fx-background-color: #FF04B1; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Transportasi")) {
                txtCategory.setStyle("-fx-background-color: #666666; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else if (item.getCategory().equals("Lain-lain")) {
                txtCategory.setStyle("-fx-background-color: #000000; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            } else {
                txtCategory.setStyle("-fx-background-color: #FF506E; -fx-background-radius: 5; -fx-opacity: 1; -fx-font-weight: bold;");
            }
            
            txtDesc.textProperty().set("Idea by " + namaUser + " | " + item.getTime());

        }
        this.businessIdea = item;
    }
}
