package ui.register;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataUser;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import xml.helper.User;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class RegisterFXMLController implements Initializable, User.UserCallback {

    @FXML
    private JFXTextField tfNama;

    @FXML
    private JFXTextField tfNoHp;

    @FXML
    private JFXTextField tfEmail;

    @FXML
    private JFXPasswordField tfPassword;

    @FXML
    private ImageView imgLogo;
    
    @FXML
    private Button btnLogin;

    private Alert alertError = new Alert(Alert.AlertType.ERROR);
    private Alert alertSuccess = new Alert(Alert.AlertType.INFORMATION);
    private User user = new User(this);

    @FXML
    private void handleButtonRegister(ActionEvent event) {
        if (tfNama.getText().isEmpty()) {
            alertError.setTitle("Field Kosong");
            alertError.setContentText("Field Nama Kosong");
            alertError.showAndWait();
        } else if (tfNoHp.getText().isEmpty()) {
            alertError.setTitle("Field Kosong");
            alertError.setContentText("Field Nomor Handphone Kosong");
            alertError.showAndWait();
        } else if (tfEmail.getText().isEmpty()) {
            alertError.setTitle("Field Kosong");
            alertError.setContentText("Field Email Kosong");
            alertError.showAndWait();
        } else if (tfPassword.getText().length() < 8) {
            alertError.setTitle("Error Password");
            alertError.setContentText("Password harus lebih dari 8 karakter!");
            alertError.showAndWait();
        } else {
            user.register(tfNama.getText(), tfNoHp.getText(), tfEmail.getText(), tfPassword.getText());
        }
    }
    
    @FXML
    private void handleButtonLogin(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/login/LoginFXML.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Login Account");
            stage.getIcons().add(new Image("/assets/Bisnisly.png"));
            stage.show();
            
            Stage stagethis = (Stage) btnLogin.getScene().getWindow();
            stagethis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

    @Override
    public void onSuccessRegisterUser(String title, String content) {
        tfNama.setText("");
        tfEmail.setText("");
        tfNoHp.setText("");
        tfPassword.setText("");
        alertSuccess.setTitle(title);
        alertSuccess.setContentText(content);
        alertSuccess.showAndWait();
    }

    @Override
    public void onErrorUser(String title, String content) {
        alertError.setTitle(title);
        alertError.setContentText(content);
        alertError.showAndWait();
    }

    @Override
    public void onCheckSessionUser(boolean session) {}

    @Override
    public void onSuccessLogin() {}

    @Override
    public void onLogoutUser() {}

    @Override
    public void onGetDataUser(ArrayList<DataUser> dataUsers) {}
}
