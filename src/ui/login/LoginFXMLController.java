package ui.login;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataUser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import xml.helper.User;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class LoginFXMLController implements Initializable, xml.helper.User.UserCallback {

    @FXML
    private JFXTextField tfNoHp;

    @FXML
    private JFXPasswordField tfPassword;

    @FXML
    private Button btnRegister;

    @FXML
    private JFXButton btnLogin;

    private Alert alertError = new Alert(Alert.AlertType.ERROR);
    private Alert alertSuccess = new Alert(Alert.AlertType.INFORMATION);
    private User user = new User(this);

    @FXML
    private void handleButtonRegister(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/register/RegisterFXML.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Register Account");
            stage.getIcons().add(new Image("/assets/Bisnisly.png"));
            stage.show();

            Stage stagethis = (Stage) btnRegister.getScene().getWindow();
            stagethis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleButtonLogin(ActionEvent event) {
        if (tfNoHp.getText().isEmpty()) {
            alertError.setTitle("Field Kosong");
            alertError.setContentText("Field Nomor Handphone Kosong");
            alertError.showAndWait();
        } else if (tfPassword.getText().isEmpty()) {
            alertError.setTitle("Field Kosong");
            alertError.setContentText("Field Password Kosong");
            alertError.showAndWait();
        } else {
            user.login(tfNoHp.getText(), tfPassword.getText());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @Override
    public void onSuccessLogin() {
        wait(3000);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/home/HomeFXML.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Bisnisly");
            stage.getIcons().add(new Image("/assets/Bisnisly.png"));
            stage.show();
            Stage stagethis = (Stage) btnLogin.getScene().getWindow();
            stagethis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorUser(String title, String content) {
        alertError.setTitle(title);
        alertError.setContentText(content);
        alertError.showAndWait();
    }

    @Override
    public void onSuccessRegisterUser(String title, String Content) {}

    @Override
    public void onCheckSessionUser(boolean session) {}

    @Override
    public void onLogoutUser() {}

    @Override
    public void onGetDataUser(ArrayList<DataUser> dataUsers) {}

    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
