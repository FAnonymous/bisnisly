package ui.splashscreen;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataUser;
import ui.home.HomeFXMLController;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import xml.helper.User;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class SplashScreenFXMLController implements Initializable, User.UserCallback {

    @FXML
    private AnchorPane layout;
    
    private User user = new User(this);
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        user.checkSession();
    }

    @Override
    public void onCheckSessionUser(boolean session) {
        if (session) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/home/HomeFXML.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.setTitle("Bisnisly");
                stage.getIcons().add(new Image("/assets/Bisnisly.png"));
                stage.close();
                stage.show();
            } catch (IOException e) {
                System.err.println(String.format("Error: %s", e.getMessage()));
            }
        } else {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/login/LoginFXML.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.setTitle("Login Account");
                stage.getIcons().add(new Image("/assets/Bisnisly.png"));
                stage.show();
            } catch (IOException e) {
                System.err.println(String.format("Error: %s", e.getMessage()));
            }
        }
    }

    @Override
    public void onErrorUser(String title, String content) {}
    
    @Override
    public void onSuccessRegisterUser(String title, String Content) {}

    @Override
    public void onSuccessLogin() {}

    @Override
    public void onLogoutUser() {}

    @Override
    public void onGetDataUser(ArrayList<DataUser> dataUsers) {}
    
}
