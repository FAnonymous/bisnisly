package ui.home.content.category;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import ui.home.HomeFXMLController;
import ui.home.content.home.HomeContentFXMLController;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class CategoryFXMLController implements Initializable {

    @FXML
    private ListView<String> listCategory;
    
    private HomeFXMLController homeFXMLController;

    ObservableList<String> dataKategori = FXCollections.observableArrayList(
            "Kuliner",
            "Financial",
            "Jasa",
            "Kesehatan",
            "Komunikasi",
            "Konstruksi",
            "Manufaktur",
            "Pertanian",
            "Perternakan",
            "Transportasi",
            "Lain-lain"
    );

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listCategory.setItems(dataKategori);
    }

    @FXML
    public void onClickList(MouseEvent arg0) {
        String category = listCategory.getSelectionModel().getSelectedItems().toString().replace("[", "").replace("]", "");
        try {
            homeFXMLController.setHome(category);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setHomeController(HomeFXMLController homeFXMLController) {
        this.homeFXMLController = homeFXMLController;
    }
}
