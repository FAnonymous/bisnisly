package ui.home.content.profile;

import data.DataSessionUser;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import xml.helper.SessionUser;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class ProfileFXMLController implements Initializable {
    
    @FXML
    private Label txtNama;
    
     @FXML
    private Label txtEmail;
     
      @FXML
    private Label txtHp;
     
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SessionUser.getInstance().setDataUser();
        txtNama.setText(DataSessionUser.getInstance().getNama());
        txtEmail.setText(DataSessionUser.getInstance().getEmail());
        txtHp.setText(DataSessionUser.getInstance().getNohp());
    }    
    
}
