package ui.home.content.home;

import adapter.home.HomeAdapterFXML;
import data.DataBusinessIdea;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import xml.helper.BusinessIdea;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class HomeContentFXMLController implements Initializable, BusinessIdea.BusinessIdeaCallback {

    @FXML
    private ListView lvHome;
    
    private BusinessIdea businessIdea = new BusinessIdea(this);
    
    private ObservableList<DataBusinessIdea> listBusinessIdeas = FXCollections.observableArrayList();
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lvHome.setCellFactory((lv) -> {
            return HomeAdapterFXML.newInstance();
        });
        lvHome.setItems(listBusinessIdeas);
    }  
    
    public void setCategory(String cat) {
        if (cat.equals("All")) {
          businessIdea.getDataAll();  
        } else {
          businessIdea.getDataByCategory(cat);
        }
    }

    @Override
    public void onSuccessGetDataBusinessIdea(ArrayList<DataBusinessIdea> dataBusinessIdeas) {
        listBusinessIdeas.clear();
        listBusinessIdeas.addAll(dataBusinessIdeas);
    }

    @Override
    public void onSuccessChangeDataBusinessIdea(String type) {
        
    }
}
