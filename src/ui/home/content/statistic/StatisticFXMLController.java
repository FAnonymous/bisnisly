package ui.home.content.statistic;

import data.DataBusinessIdea;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import xml.helper.BusinessIdea;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class StatisticFXMLController implements Initializable, BusinessIdea.BusinessIdeaCallback {
    
    private BusinessIdea businessIdea = new BusinessIdea(this);
    ObservableList<PieChart.Data> data = FXCollections.observableArrayList();
    
    @FXML
    PieChart pcMyIdea;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        businessIdea.getDataAll();
    }    

    @Override
    public void onSuccessGetDataBusinessIdea(ArrayList<DataBusinessIdea> dataBusinessIdeas) {
        data.clear();
        int kuliner = 0;
        int financial = 0;
        int jasa = 0;
        int kesehatan = 0;
        int komunikasi = 0;
        int konstruksi = 0;
        int manufaktur = 0;
        int pertanian = 0;
        int perternakan = 0;
        int transportasi = 0;
        int lainlain = 0;
        for (int i = 0; i < dataBusinessIdeas.size(); i++) {
            if (dataBusinessIdeas.get(i).getCategory().equals("Kuliner")) {
                kuliner = kuliner + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Financial")) {
                financial = financial + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Jasa")) {
                jasa = jasa + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Kesehatan")) {
                kesehatan = kesehatan + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Komunikasi")) {
                komunikasi = komunikasi + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Konstruksi")) {
                konstruksi = konstruksi + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Manufaktur")) {
                manufaktur = manufaktur + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Pertanian")) {
                pertanian = pertanian + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Perternakan")) {
                perternakan = perternakan + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Transportasi")) {
                transportasi = transportasi + 1;
            } else if (dataBusinessIdeas.get(i).getCategory().equals("Lain-lain")) {
                lainlain = lainlain + 1;
            } else {
                lainlain = lainlain + 1;
            }
        }
        
        data.add(new PieChart.Data("Kuliner", kuliner));
        data.add(new PieChart.Data("Financial", financial));
        data.add(new PieChart.Data("Jasa", jasa));
        data.add(new PieChart.Data("Kesehatan", kesehatan));
        data.add(new PieChart.Data("Komunikasi", komunikasi));
        data.add(new PieChart.Data("Konstruksi", konstruksi));
        data.add(new PieChart.Data("Manufaktur", manufaktur));
        data.add(new PieChart.Data("Pertanian", pertanian));
        data.add(new PieChart.Data("Perternakan", perternakan));
        data.add(new PieChart.Data("Transportasi", transportasi));
        data.add(new PieChart.Data("Lain-lain", lainlain));
        pcMyIdea.setData(data);
        
        data.forEach(data ->
                data.nameProperty().bind(
                        Bindings.concat(
                                data.getName(), " (", data.pieValueProperty(), ")"
                        )
                )
        );
    }

    @Override
    public void onSuccessChangeDataBusinessIdea(String type) {
        
    }
    
}
