package ui.home.content.myidea.change;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import data.DataBusinessIdea;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import ui.home.content.myidea.MyIdeaContentFXMLController;
import xml.helper.BusinessIdea;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class AddEditMyIdeaFXMLController implements Initializable, BusinessIdea.BusinessIdeaCallback {

    @FXML
    private JFXTextArea edtContent;

    @FXML
    private JFXTextField edtTitle;

    @FXML
    private JFXComboBox<String> cbCategory;

    @FXML
    private JFXButton btnChange;

    private int id, userId;
    private String type, title, content, category;
    private Stage stage;
    private BusinessIdea businessIdea = new BusinessIdea(this);
    private MyIdeaContentFXMLController myIdeaContentFXMLController;

    private Alert alertError = new Alert(Alert.AlertType.ERROR);
    private Alert alertSuccess = new Alert(Alert.AlertType.INFORMATION);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbCategory.getItems().add("Kuliner");
        cbCategory.getItems().add("Financial");
        cbCategory.getItems().add("Jasa");
        cbCategory.getItems().add("Kesehatan");
        cbCategory.getItems().add("Komunikasi");
        cbCategory.getItems().add("Konstruksi");
        cbCategory.getItems().add("Manufaktur");
        cbCategory.getItems().add("Pertanian");
        cbCategory.getItems().add("Perternakan");
        cbCategory.getItems().add("Transportasi");
        cbCategory.getItems().add("Lain-lain");
    }

    @FXML
    private void onBtnChange(ActionEvent event) {
        stage = (Stage) btnChange.getScene().getWindow();
        if (edtTitle.getText().isEmpty()) {
            alertError.setTitle("Kosong!!!");
            alertError.setContentText("Judul Masih Kosong");
            alertError.showAndWait();
        } else if (edtContent.getText().isEmpty()) {
            alertError.setTitle("Kosong!!!");
            alertError.setContentText("Isi Ide Masih Kosong");
            alertError.showAndWait();
        } else if (cbCategory.getValue() == null) {
            alertError.setTitle("Kosong!!!");
            alertError.setContentText("Belum Pilih Kategori");
            alertError.showAndWait();
        } else {
            if (type.equals("Edit")) {
                businessIdea.edit(id, edtTitle.getText(), edtContent.getText(), cbCategory.getValue());
            } else {
                String time = ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME);
                int lengthForTime = 0;
                String timeFormat = "";
                if (time.length() == 30) {
                    lengthForTime = 21;
                } else {
                    lengthForTime = 22;
                }
                for (int i = 0; i < lengthForTime; i++) {
                    timeFormat = timeFormat + time.charAt(i);
                }
                businessIdea.add(userId, edtTitle.getText(), edtContent.getText(), cbCategory.getValue(), timeFormat);
            }
        }

    }

    public void setData(String type, int id, int userId, String title, String content, String category, MyIdeaContentFXMLController myIdeaContentFXMLController) {
        this.type = type;
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.category = category;
        this.myIdeaContentFXMLController = myIdeaContentFXMLController;

        if (type.equals("Edit")) {
            edtTitle.setText(title);
            edtContent.setText(content);
            cbCategory.setValue(category);
        }
        cbCategory.setVisibleRowCount(3);
        btnChange.setText(type + " My Idea");
    }

    @Override
    public void onSuccessGetDataBusinessIdea(ArrayList<DataBusinessIdea> dataBusinessIdeas) {
    }

    @Override
    public void onSuccessChangeDataBusinessIdea(String type) {
        myIdeaContentFXMLController.refreshData();
        alertSuccess.setTitle("Berhasil");
        alertSuccess.setContentText("Data berhasil ditambah/diubah");
        alertSuccess.showAndWait();
        stage.close();
    }

}
