package ui.home.content.myidea;

import com.jfoenix.controls.JFXNodesList;
import data.DataBusinessIdea;
import data.DataSessionUser;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ui.home.content.myidea.change.AddEditMyIdeaFXMLController;
import util.ActionButtonTableCell;
import xml.helper.BusinessIdea;
import xml.helper.SessionUser;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class MyIdeaContentFXMLController implements Initializable, BusinessIdea.BusinessIdeaCallback {

    @FXML
    private TableView tfBusinessIdea;

    @FXML
    private TableColumn tcId;

    @FXML
    private TableColumn tcCategory;

    @FXML
    private TableColumn tcTitle;

    @FXML
    private TableColumn tcContent;

    @FXML
    private TableColumn tcTime;

    @FXML
    private TableColumn tcEdit;

    @FXML
    private TableColumn tcDelete;

    private BusinessIdea businessIdea = new BusinessIdea(this);
    private ObservableList<DataBusinessIdea> listBusinessIdeas = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SessionUser.getInstance().setDataUser();

        tcId.setCellValueFactory(new PropertyValueFactory<DataBusinessIdea, String>("id"));
        tcCategory.setCellValueFactory(new PropertyValueFactory<DataBusinessIdea, String>("category"));
        tcTitle.setCellValueFactory(new PropertyValueFactory<DataBusinessIdea, String>("title"));
        tcContent.setCellValueFactory(new PropertyValueFactory<DataBusinessIdea, String>("content"));
        tcTime.setCellValueFactory(new PropertyValueFactory<DataBusinessIdea, String>("time"));

        tcEdit.setCellFactory(ActionButtonTableCell.<DataBusinessIdea>forTableColumn("Edit", (data) -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/home/content/myidea/change/AddEditMyIdeaFXML.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();

                AddEditMyIdeaFXMLController addEditMyIdeaFXMLController = fxmlLoader.getController();
                addEditMyIdeaFXMLController.setData("Edit", data.getId(), data.getUserId(), data.getTitle(), data.getContent(), data.getCategory(), MyIdeaContentFXMLController.this);

                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.setTitle("Edit My Idea Bisnis");
                stage.getIcons().add(new Image("/assets/Bisnisly.png"));
                stage.show();
            } catch (IOException e) {
                System.err.println(String.format("Error: %s", e.getMessage()));
            }
            return data;
        }));

        tcDelete.setCellFactory(ActionButtonTableCell.<DataBusinessIdea>forTableColumn("Delete", (data) -> {
            businessIdea.delete(data.getId());
            return data;
        }));

        tfBusinessIdea.setItems(listBusinessIdeas);

        businessIdea.getDataByIdUser(DataSessionUser.getInstance().getId());
    }

    @FXML
    private void onBtnAdd(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/home/content/myidea/change/AddEditMyIdeaFXML.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();

            AddEditMyIdeaFXMLController addEditMyIdeaFXMLController = fxmlLoader.getController();
            addEditMyIdeaFXMLController.setData("Add", 0, DataSessionUser.getInstance().getId(), "", "", "", MyIdeaContentFXMLController.this);

            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Add My Idea Bisnis");
            stage.getIcons().add(new Image("/assets/Bisnisly.png"));
            stage.show();
        } catch (IOException e) {
            System.err.println(String.format("Error: %s", e.getMessage()));
        }
    }

    @Override
    public void onSuccessGetDataBusinessIdea(ArrayList<DataBusinessIdea> dataBusinessIdeas) {
        listBusinessIdeas.clear();
        listBusinessIdeas.addAll(dataBusinessIdeas);
    }

    @Override
    public void onSuccessChangeDataBusinessIdea(String type) {
        businessIdea.getDataByIdUser(DataSessionUser.getInstance().getId());
    }
    
    public void refreshData() {
        businessIdea.getDataByIdUser(DataSessionUser.getInstance().getId());
    }
}
