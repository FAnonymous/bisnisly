package ui.home;

import com.jfoenix.controls.JFXButton;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataSessionUser;
import data.DataUser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import ui.home.content.category.CategoryFXMLController;
import ui.home.content.home.HomeContentFXMLController;
import xml.helper.SessionUser;
import xml.helper.User;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */
public class HomeFXMLController implements Initializable, User.UserCallback {

    @FXML
    private JFXButton btnLogout;
    
    @FXML
    private Label txtName;
    
    @FXML
    private AnchorPane layoutMain;
    
    @FXML
    private BorderPane borderpane;

    private User user = new User(this);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SessionUser.getInstance().setDataUser();
        txtName.setText(DataSessionUser.getInstance().getNama().toUpperCase());
        setHome("All");
    }
    
    @FXML
    private void onBtnLogout(ActionEvent event) {
        user.logout();
    }
    
    @FXML
    private void onBtnHome(ActionEvent event) {
        setHome("All");
    }
    
    @FXML
    private void onBtnCategory(ActionEvent event) {
        try {
            FXMLLoader object = new FXMLLoader(getClass().getResource("/ui/home/content/category/CategoryFXML.fxml"));
            Pane view = object.load();
            CategoryFXMLController categoryFXMLController = object.getController();
            categoryFXMLController.setHomeController(HomeFXMLController.this);
            borderpane.setCenter(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void onBtnAbout(ActionEvent event) {         
        loadFXML("/ui/home/content/about/AboutContentFXML.fxml");
    }
    
    @FXML
    private void onBtnMyIdea(ActionEvent event) {
        loadFXML("/ui/home/content/myidea/MyIdeaContentFXML.fxml");
    }
    
    @FXML
    private void onBtnProfile() {
        loadFXML("/ui/home/content/profile/ProfileFXML.fxml");
    }
    
    @FXML
    private void onBtnStatistic() {
        loadFXML("/ui/home/content/statistic/StatisticFXML.fxml");
    }

    @Override
    public void onLogoutUser() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ui/login/LoginFXML.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Login Account");
            stage.show();
            Stage stagethis = (Stage) btnLogout.getScene().getWindow();
            stagethis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void loadFXML(String layout) {
        try {
            FXMLLoader object = new FXMLLoader();
            Pane view = object.load(getClass().getResource(layout));
            borderpane.setCenter(view);   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
    @Override
    public void onGetDataUser(ArrayList<DataUser> dataUsers) {}
    
    @Override
    public void onSuccessRegisterUser(String title, String Content) {}

    @Override
    public void onSuccessLogin() {}

    @Override
    public void onCheckSessionUser(boolean session) {}

    @Override
    public void onErrorUser(String title, String content) {}

    public void setHome(String category) {
        try {
            FXMLLoader object = new FXMLLoader(getClass().getResource("/ui/home/content/home/HomeContentFXML.fxml"));
            Pane view = object.load();
            HomeContentFXMLController homeContentFXMLController = object.getController();
            homeContentFXMLController.setCategory(category);
            borderpane.setCenter(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
