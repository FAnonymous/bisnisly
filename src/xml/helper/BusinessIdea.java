package xml.helper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataBusinessIdea;
import data.DataComment;
import data.DataUser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class BusinessIdea {

    public interface BusinessIdeaCallback {

        void onSuccessGetDataBusinessIdea(ArrayList<DataBusinessIdea> dataBusinessIdeas);

        void onSuccessChangeDataBusinessIdea(String type);

    }

    private BusinessIdeaCallback businessIdeaCallback;

    public BusinessIdea(BusinessIdeaCallback businessIdeaCallback) {
        this.businessIdeaCallback = businessIdeaCallback;
    }

    public void getDataAll() {
        ArrayList<DataBusinessIdea> dataBusinessIdeas = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeas = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        businessIdeaCallback.onSuccessGetDataBusinessIdea(dataBusinessIdeas);
    }

    public void getDataByIdUser(int id) {
        ArrayList<DataBusinessIdea> dataBusinessIdeas = new ArrayList<>();
        ArrayList<DataBusinessIdea> dataByIdUser = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeas = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataBusinessIdeas.size(); i++) {
            if (id == dataBusinessIdeas.get(i).getUserId()) {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(dataBusinessIdeas.get(i).getId());
                data.setUserId(dataBusinessIdeas.get(i).getUserId());
                data.setTitle(dataBusinessIdeas.get(i).getTitle());
                data.setContent(dataBusinessIdeas.get(i).getContent());
                data.setCategory(dataBusinessIdeas.get(i).getCategory());
                data.setTime(dataBusinessIdeas.get(i).getTime());
                dataByIdUser.add(data);
            }
        }

        businessIdeaCallback.onSuccessGetDataBusinessIdea(dataByIdUser);
    }
    
    public void getDataByCategory(String category) {
        System.out.println(category);
        ArrayList<DataBusinessIdea> dataBusinessIdeas = new ArrayList<>();
        ArrayList<DataBusinessIdea> dataByIdUser = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeas = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataBusinessIdeas.size(); i++) {
            if (category.equals(dataBusinessIdeas.get(i).getCategory())) {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(dataBusinessIdeas.get(i).getId());
                data.setUserId(dataBusinessIdeas.get(i).getUserId());
                data.setTitle(dataBusinessIdeas.get(i).getTitle());
                data.setContent(dataBusinessIdeas.get(i).getContent());
                data.setCategory(dataBusinessIdeas.get(i).getCategory());
                data.setTime(dataBusinessIdeas.get(i).getTime());
                dataByIdUser.add(data);
            }
        }

        businessIdeaCallback.onSuccessGetDataBusinessIdea(dataByIdUser);
    }

    public void add(int userId, String title, String content, String category, String time) {
        ArrayList<DataBusinessIdea> dataBusinessIdeasGet = new ArrayList<>();
        ArrayList<DataBusinessIdea> dataBusinessIdeasAdd = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeasGet = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        int size = dataBusinessIdeasGet.size();
        int id = 0;

        if (size == 0) {
            DataBusinessIdea data = new DataBusinessIdea();
            data.setId(1);
            data.setUserId(userId);
            data.setTitle(title);
            data.setContent(content);
            data.setCategory(category);
            data.setTime(time);
            dataBusinessIdeasAdd.add(data);
        } else {
            for (int i = 0; i < dataBusinessIdeasGet.size(); i++) {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(dataBusinessIdeasGet.get(i).getId());
                data.setUserId(dataBusinessIdeasGet.get(i).getUserId());
                data.setTitle(dataBusinessIdeasGet.get(i).getTitle());
                data.setContent(dataBusinessIdeasGet.get(i).getContent());
                data.setCategory(dataBusinessIdeasGet.get(i).getCategory());
                data.setTime(dataBusinessIdeasGet.get(i).getTime());
                dataBusinessIdeasAdd.add(data);
                id = dataBusinessIdeasGet.get(i).getId();
            }

            DataBusinessIdea data = new DataBusinessIdea();
            id = id + 1;
            data.setId(id);
            data.setUserId(userId);
            data.setTitle(title);
            data.setContent(content);
            data.setCategory(category);
            data.setTime(time);
            dataBusinessIdeasAdd.add(data);
        }

        String xml = xStream.toXML(dataBusinessIdeasAdd);
        FileOutputStream fileOutputStream;

        try {
            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/DataBusinessIdea.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        businessIdeaCallback.onSuccessChangeDataBusinessIdea("Add");
    }

    public void edit(int id, String title, String content, String category) {
        ArrayList<DataBusinessIdea> dataBusinessIdeasGet = new ArrayList<>();
        ArrayList<DataBusinessIdea> dataBusinessIdeasEdit = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeasGet = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataBusinessIdeasGet.size(); i++) {
            if (id == dataBusinessIdeasGet.get(i).getId()) {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(id);
                data.setUserId(dataBusinessIdeasGet.get(i).getUserId());
                data.setTitle(title);
                data.setContent(content);
                data.setCategory(category);
                data.setTime(dataBusinessIdeasGet.get(i).getTime());
                dataBusinessIdeasEdit.add(data);
            } else {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(dataBusinessIdeasGet.get(i).getId());
                data.setUserId(dataBusinessIdeasGet.get(i).getUserId());
                data.setTitle(dataBusinessIdeasGet.get(i).getTitle());
                data.setContent(dataBusinessIdeasGet.get(i).getContent());
                data.setCategory(dataBusinessIdeasGet.get(i).getCategory());
                data.setTime(dataBusinessIdeasGet.get(i).getTime());
                dataBusinessIdeasEdit.add(data);
            }
        }

        String xml = xStream.toXML(dataBusinessIdeasEdit);
        FileOutputStream fileOutputStream;

        try {
            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/DataBusinessIdea.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        businessIdeaCallback.onSuccessChangeDataBusinessIdea("Edit");
    }

    public void delete(int id) {
        ArrayList<DataBusinessIdea> dataBusinessIdeasGet = new ArrayList<>();
        ArrayList<DataBusinessIdea> dataBusinessIdeasDelete = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataBusinessIdea.xml");
            dataBusinessIdeasGet = (ArrayList<DataBusinessIdea>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataBusinessIdeasGet.size(); i++) {
            if (id == dataBusinessIdeasGet.get(i).getId()) {
                
            } else {
                DataBusinessIdea data = new DataBusinessIdea();
                data.setId(dataBusinessIdeasGet.get(i).getId());
                data.setUserId(dataBusinessIdeasGet.get(i).getUserId());
                data.setTitle(dataBusinessIdeasGet.get(i).getTitle());
                data.setContent(dataBusinessIdeasGet.get(i).getContent());
                data.setCategory(dataBusinessIdeasGet.get(i).getCategory());
                data.setTime(dataBusinessIdeasGet.get(i).getTime());
                dataBusinessIdeasDelete.add(data);
            }
        }

        String xml = xStream.toXML(dataBusinessIdeasDelete);
        FileOutputStream fileOutputStream;

        try {
            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/DataBusinessIdea.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        ArrayList<DataComment> dataCommentsGet = new ArrayList<>();
        ArrayList<DataComment> dataCommentsDelete = new ArrayList<>();
        XStream xStreamComment = new XStream(new StaxDriver());
        FileInputStream fileInputStreamComment;
        try {
            fileInputStreamComment = new FileInputStream("src/xml/DataComment.xml");
            dataCommentsGet = (ArrayList<DataComment>) xStream.fromXML(fileInputStreamComment);
            fileInputStreamComment.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataCommentsGet.size(); i++) {
            if (id == dataCommentsGet.get(i).getIdBusiness()) {
                
            } else {
                DataComment data = new DataComment();
                data.setId(dataCommentsGet.get(i).getId());
                data.setIdUser(dataCommentsGet.get(i).getIdUser());
                data.setIdBusiness(dataCommentsGet.get(i).getIdBusiness());
                data.setCommnent(dataCommentsGet.get(i).getCommnent());
                dataCommentsDelete.add(data);
            }
        }

        String xmlComment = xStream.toXML(dataCommentsDelete);
        FileOutputStream fileOutputStreamComment;

        try {
            byte[] data = xmlComment.getBytes("UTF-8");
            fileOutputStreamComment = new FileOutputStream("src/xml/DataComment.xml");
            fileOutputStreamComment.write(data);
            fileOutputStreamComment.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        businessIdeaCallback.onSuccessChangeDataBusinessIdea("Delete");
    }
}
