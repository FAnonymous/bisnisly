package xml.helper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataSessionUser;
import data.DataUser;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class SessionUser {

    private static SessionUser mInstance;

    public SessionUser() {
        
    }

    public static SessionUser getInstance() {
        if (mInstance == null) {
            mInstance = new SessionUser();
        }
        return mInstance;
    }
    
    public void setDataUser() {
        ArrayList<DataUser> dataUsers = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/SessionUser.xml");
            dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }
        
        for (int i = 0; i < dataUsers.size(); i++) {
            DataSessionUser.getInstance().setId(dataUsers.get(i).getId());
            DataSessionUser.getInstance().setNama(dataUsers.get(i).getNama());
            DataSessionUser.getInstance().setNohp(dataUsers.get(i).getNohp());
            DataSessionUser.getInstance().setEmail(dataUsers.get(i).getEmail());
            DataSessionUser.getInstance().setPassword(dataUsers.get(i).getPassword());
        }
    }
}
