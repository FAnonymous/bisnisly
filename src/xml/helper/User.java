package xml.helper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataUser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class User {
    
    public interface UserCallback {
        
        void onSuccessRegisterUser(String title, String Content);
        
        void onSuccessLogin();
        
        void onCheckSessionUser(boolean session);
        
        void onErrorUser(String title, String content);
        
        void onLogoutUser();
        
        void onGetDataUser(ArrayList<DataUser> dataUsers);
                
    }

    private UserCallback userCallback;

    public User(UserCallback userCallback) {
        this.userCallback = userCallback;
    }  
    
    public void register(String nama, String nohp, String email, String password) {
        boolean checkData = false;
        ArrayList<DataUser> dataUsers = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataUser.xml");
            dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataUsers.size(); i++) {
            DataUser dataUser = dataUsers.get(i);
            if (nohp.equalsIgnoreCase(dataUser.getNohp()) || email.equalsIgnoreCase(dataUser.getEmail())) {
                checkData = true;
            }
        }

        if (checkData) {
            userCallback.onErrorUser("Gagal Register", "Nomor Handphone atau Email sudah terdaftar");
        } else {
            registerUser(dataUsers, nama, nohp, email, password);
        }
    }
    
    private void registerUser(ArrayList<DataUser> dataUsers, String nama, String nohp, String email, String password) {
        int id = dataUsers.size();
        ArrayList<DataUser> dataUsersAdd = new ArrayList<>();
        
        XStream xStream = new XStream(new StaxDriver());

        if (id == 0) {
            DataUser dataUser = new DataUser();
            dataUser.setId(1);
            dataUser.setNama(nama);
            dataUser.setNohp(nohp);
            dataUser.setEmail(email);
            dataUser.setPassword(password);
            dataUsersAdd.add(dataUser);

        } else {
            for (int i = 0; i < dataUsers.size(); i++) {
                DataUser dataUser = new DataUser();
                dataUser.setId(dataUsers.get(i).getId());
                dataUser.setNama(dataUsers.get(i).getNama());
                dataUser.setNohp(dataUsers.get(i).getNohp());
                dataUser.setEmail(dataUsers.get(i).getEmail());
                dataUser.setPassword(dataUsers.get(i).getPassword());
                dataUsersAdd.add(dataUser);
            }
            
            DataUser dataUser = new DataUser();
            dataUser.setId(id + 1);
            dataUser.setNama(nama);
            dataUser.setNohp(nohp);
            dataUser.setEmail(email);
            dataUser.setPassword(password);
            dataUsersAdd.add(dataUser);
        }

        String xml = xStream.toXML(dataUsersAdd);
        FileOutputStream fileOutputStream;

        try {
            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/DataUser.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
            userCallback.onSuccessRegisterUser("Berhasil", "Selamat " + nama + ", Kamu Berhasil Register Akun Bisnisly");
        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }
    }
    
    public void checkSession() {
        boolean checkUser = false;
        ArrayList<DataUser> dataUsers = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/SessionUser.xml");
            dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataUsers.size(); i++) {
            DataUser dataUser = dataUsers.get(i);
            if (dataUser.getId() > 0) {
                checkUser = true;
            }
        }
        userCallback.onCheckSessionUser(checkUser);
    }
    
    public void login(String nohp, String pass) {
        boolean checkData = false;
        int id = 0;
        String nama = "", email = "";
        ArrayList<DataUser> dataUsers = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataUser.xml");
            dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataUsers.size(); i++) {
            DataUser dataUser = dataUsers.get(i);
            if (nohp.equals(dataUser.getNohp()) && pass.equals(dataUser.getPassword())) {
                checkData = true;
                id = dataUser.getId();
                nama = dataUser.getNama();
                email = dataUser.getEmail();
            }
        }

        if (checkData) {
            try {
                ArrayList<DataUser> sessionUsersAdd = new ArrayList<>();
                DataUser dataUser = new DataUser();
                dataUser.setId(id);
                dataUser.setNama(nama);
                dataUser.setNohp(nohp);
                dataUser.setEmail(email);
                dataUser.setPassword(pass);
                sessionUsersAdd.add(dataUser);

                String xml = xStream.toXML(sessionUsersAdd);
                FileOutputStream fileOutputStream;

                byte[] data = xml.getBytes("UTF-8");
                fileOutputStream = new FileOutputStream("src/xml/SessionUser.xml");
                fileOutputStream.write(data);
                fileOutputStream.close();
                
                userCallback.onSuccessLogin();
                
            } catch (IOException e) {
                System.err.println(String.format("Error: %s", e.getMessage()));
            }
        } else {
            userCallback.onErrorUser("Gagal Login", "Nomor Handphone atau Email salah");
        }
    }
    
    public void logout() {
        try {
            ArrayList<DataUser> sessionUsersAdd = new ArrayList<>();
            XStream xStream = new XStream(new StaxDriver());
            DataUser dataUser = new DataUser();
            dataUser.setId(0);
            dataUser.setNama("");
            dataUser.setNohp("");
            dataUser.setEmail("");
            dataUser.setPassword("");
            sessionUsersAdd.add(dataUser);

            String xml = xStream.toXML(sessionUsersAdd);
            FileOutputStream fileOutputStream;

            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/SessionUser.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
            userCallback.onLogoutUser();
        } catch (IOException e) {
            System.err.println(String.format("Error: %s", e.getMessage()));
        }
    }
    
    public void getDataUser() {
        ArrayList<DataUser> dataUsers = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/SessionUser.xml");
            dataUsers = (ArrayList<DataUser>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }
        
        userCallback.onGetDataUser(dataUsers);
    }
}