package xml.helper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.DataComment;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * @author Ahmad Faishal Albadri
 * @nim 20523166
 * 
 * @author Khalik Trinoor R
 * @nim 20523178
 * 
 * @author Kurnia Akbar
 * @nim 20523057
 */

public class Comment {

    public interface CommentCallback {

        void onSuccesGetCommentCallback(ArrayList<DataComment> dataComments);

        void onSuccesChangeCommentCallback(String type);

    }

    private CommentCallback commentCallback;

    public Comment(CommentCallback commentCallback) {
        this.commentCallback = commentCallback;
    }
    
    public void getDataByIdUser(int id) {
        ArrayList<DataComment> dataComments = new ArrayList<>();
        ArrayList<DataComment> dataByIdBusiness = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataComment.xml");
            dataComments = (ArrayList<DataComment>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        for (int i = 0; i < dataComments.size(); i++) {
            if (id == dataComments.get(i).getIdBusiness()) {
                DataComment data = new DataComment();
                data.setId(dataComments.get(i).getId());
                data.setIdBusiness(dataComments.get(i).getIdBusiness());
                data.setIdUser(dataComments.get(i).getId());
                data.setCommnent(dataComments.get(i).getCommnent());
                dataByIdBusiness.add(data);
            }
        }

        commentCallback.onSuccesGetCommentCallback(dataByIdBusiness);
    }
    
    public void add(int idUser, int idBusiness, String comment) {
        ArrayList<DataComment> dataCommentsGet = new ArrayList<>();
        ArrayList<DataComment> dataCommentsAdd = new ArrayList<>();
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream("src/xml/DataComment.xml");
            dataCommentsGet = (ArrayList<DataComment>) xStream.fromXML(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            System.err.println("Terjadi Kesalahan: " + e.getMessage());
        }

        int size = dataCommentsGet.size();
        int id = 0;

        if (size == 0) {
            DataComment data = new DataComment();
            data.setId(1);
            data.setIdUser(idUser);
            data.setIdBusiness(idBusiness);
            data.setCommnent(comment);
            dataCommentsAdd.add(data);
        } else {
            for (int i = 0; i < dataCommentsGet.size(); i++) {
                DataComment data = new DataComment();
                data.setId(dataCommentsGet.get(i).getId());
                data.setIdUser(dataCommentsGet.get(i).getIdUser());
                data.setIdBusiness(dataCommentsGet.get(i).getIdBusiness());
                data.setCommnent(dataCommentsGet.get(i).getCommnent());
                dataCommentsAdd.add(data);
                id = dataCommentsGet.get(i).getId();
            }

            DataComment data = new DataComment();
            id = id + 1;
            data.setId(id);
            data.setIdUser(idUser);
            data.setIdBusiness(idBusiness);
            data.setCommnent(comment);
            dataCommentsAdd.add(data);
        }

        String xml = xStream.toXML(dataCommentsAdd);
        FileOutputStream fileOutputStream;

        try {
            byte[] data = xml.getBytes("UTF-8");
            fileOutputStream = new FileOutputStream("src/xml/DataComment.xml");
            fileOutputStream.write(data);
            fileOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        commentCallback.onSuccesChangeCommentCallback("Add");
    }
}
